void main() {
  
          print('** welcome to our Library **');
          print('---------------------');
          print('this is list of our books:');
  
  List<Book> books = [
    Book(
        name: "the lord of the rings",
        author: "Tolkien",
        year: 1954),
    Book(
        name: "harry potter",
        author: "Rowling",
        year: 1998),
    Book(
        name: "les miserables",
        author: "victor Hugo",
        year: 1862),
    Book(
        name: "a tale of two cities",
        author: "charles Dickens",
        year: 1859),
  ];
  for (var item in books) {
    print('"${item.name}" ' 'by ''${item.author} ''(${item.year})');}
  
  
          print('---------------------');
          print('*this books was added:');
  
  
 Library myLibrary = Library();
    myLibrary.addBook ("1984","orwell",1944);
    myLibrary.addBook("the great Gatsby", "Fitzgerald" , 1925);
  
  
          print('---------------------');
          print('*there is new list of our books:'); 
  
  
  Library display= Library();
    display.displayBooks("the lord of the rings","Tolkien",1954);
    display.displayBooks("harry potter","Rowling",1998);
    display.displayBooks("les miserables","victor Hugo",1862);
    display.displayBooks("a tale of two cities","charles Dickens",1859);
    display.displayBooks("1984","orwell",1944);
    display.displayBooks("the great Gatsby", "Fitzgerald" , 1925);
  
  
          print('---------------------');
          print('*this books was removed:');
  
  
  Library removed= Library();
  removed.removeBooks('"the great Gatsby"');
  
  
          print('---------------------');
  
  Library search1= Library();
  search1.searchBooks1("tale");
  print(search1);
  
  Library search2= Library();
  search2.searchBook2("Rowling");
  print(search2);
}


class Book {
  String name;
  String author;
  int year;

  Book({
    required this.name,
    required this.author,
    required this.year,
  });
}

class Library{
     
    List<Book>? books;
  
   Library({
    this.books,
   });
  
    void addBook (String name, String author, int year) {
      print("Book added: $name by $author, published in $year :)") ;
    }  
  
    void displayBooks (String name, String author, int year){
      print("$name by $author, published in $year");
    }
    
    String searchBooks1 (String name){
       return "search results for '$name'";
    }
    
    String searchBook2 ( String author){
       return("search results for '$author'");
    }
    void removeBooks (String name){
      print("$name was removed!");
    }
}
